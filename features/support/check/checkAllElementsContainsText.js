const assert = require('assert').strict;

/**
 * Checks if an element contains the given text.
 * @param {String} selector CSS selector of the element to check for the given text.
 * @param {String} not String "not" to indicate that the element should not have the given text.
 * @param {String} expectedText Text to check if the given element contains.
 */
module.exports = async function (selector, not, expectedText) {
    /* istanbul ignore next */  // Required otherwise code coverage evaluation fails within $eval calls

    const list = await this.page.$$(selector);
    let texts = [];
    for (let i = 0; i < list.length; i++) {
        texts.push(await (await list[i].getProperty('textContent')).jsonValue())
    }

    const shouldContainText = not ? false : true;

    for (let i = 0; i<texts.length;i++) {
        var containsText = texts[i] && texts[i].includes(expectedText);
        var textValue = texts[i]

        if (containsText === true) {
            break
        }
    }

    assert.strictEqual(containsText, shouldContainText, `Expected "${selector}" to ${shouldContainText ? 'contain' : 'not contain'} "${expectedText}" but had "${textValue}" instead`);

}
