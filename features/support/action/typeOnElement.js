/**
 * Type text on the keyboard
 * @param {String} string The key to press
 * @param {String} selector The element to focus before pressing the key
 */
module.exports = async function(string, selector) {
    if(selector){
        await this.page.focus(selector);
    }
    await this.page.keyboard.type(string);
};