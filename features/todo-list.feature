Feature: Todo List Acceptance
    Scenario: Todo Listeyi Bul
        Given the url "http://localhost:3000" is opened

        Given the element "ul" is empty
        When I type "buy some milk" on the input "#input"
        When I click the button "#add-btn"
        Then I expect the element "li:last-child" contains text "buy some milk"

        Given the element "ul>li:first-child" contains text "buy some milk"
        When I type "enjoy the assignment" on the input "#input"
        When I click the button "#add-btn"
        Then I expect the element "li:last-child" contains text "enjoy the assignment"

        Given the elements "ul>li" contains text "enjoy the assignment"



